
### Project description

The scope of this project is to introduce you in the world of automated testing using Selenium 2 (aka. Selenium WebDriver)

http://www.seleniumhq.org/docs/

### Tested apps
We are testing two simple web apps:
https://posabogdan.tech/demo/selenium/add.html

https://posabogdan.tech/demo/selenium/add_bug.html

Each app implements a simple logic which consist in adding together two numbers.
The last one contains a bug that needs to be detected

### Project Setup
Download the [ChromeDriver - WebDriver for Chrome] (https://sites.google.com/a/chromium.org/chromedriver/downloads)

Change the code accordingly (replace the path):

'''java
System.setProperty("webdriver.chrome.driver","/home/posa/Downloads/chromedriver_linux64/chromedriver");
'''